import React from 'react';
import MainLayout from '../layouts/MainLayout';

import ImgService from '../assets/images/img/img_service.png';
import ImgWhy1 from '../assets/images/img/icon_complete.png';
import ImgWhy2 from '../assets/images/img/icon_price.png';
import ImgWhy3 from '../assets/images/img/icon_24hrs.png';
import ImgWhy4 from '../assets/images/img/icon_professional.png';
import Hero from '../components/Hero';
import Testi from '../components/Testi';

const Home = () => {
  return (
    <>
      <MainLayout>
        <Hero />
        <div class="container" id="our_services">
          <div class="row justify-content-center align-items-center">
            <div class="col">
              <img src={ImgService} alt="" />
            </div>
            <div class="col sentense">
              <h5 class="mb-4">
                Best Car Rental for any kind of trip in <br />
                Lombok Tengah!
              </h5>
              <p class="mb-4">
                Sewa mobil di Lombok Tengah bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
              </p>
              <div class="d-flex align-items-cente flex-rowr">
                <div>
                  <span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.3333 4L5.99996 11.3333L2.66663 8" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  </span>
                </div>
                <div>
                  <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                </div>
              </div>
              <div class="d-flex align-items-cente flex-rowr">
                <div>
                  <span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.3333 4L5.99996 11.3333L2.66663 8" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  </span>
                </div>
                <div>
                  <p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                </div>
              </div>
              <div class="d-flex align-items-cente flex-rowr">
                <div>
                  <span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.3333 4L5.99996 11.3333L2.66663 8" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  </span>
                </div>
                <div>
                  <p>Sewa Mobil Jangka Panjang Bulanan</p>
                </div>
              </div>
              <div class="d-flex align-items-cente flex-rowr">
                <div>
                  <span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.3333 4L5.99996 11.3333L2.66663 8" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  </span>
                </div>
                <div>
                  <p>Gratis Antar - Jemput Mobil di Bandara</p>
                </div>
              </div>
              <div class="d-flex align-items-cente flex-rowr">
                <div>
                  <span>
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.3333 4L5.99996 11.3333L2.66663 8" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                  </span>
                </div>
                <div>
                  <p>Layanan Airport Transfer / Drop In Out</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container" id="why_us">
          <h5>Why Us?</h5>
          <p>Mengapa harus pilih Binar Car Rental?</p>
          <div class="row justify-content-center">
            <div class="col-lg-3">
              <div class="card">
                <div class="card-body">
                  <p class="first">
                    <img src={ImgWhy1} alt="" />
                  </p>
                  <h5>Mobil Lengkap</h5>
                  <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="card">
                <div class="card-body">
                  <p class="second">
                    <img src={ImgWhy2} alt="" />
                  </p>
                  <h5>Harga Murah</h5>
                  <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="card">
                <div class="card-body">
                  <p class="third">
                    <img src={ImgWhy3} alt="" />
                  </p>
                  <h5>Layanan 24 Jam</h5>
                  <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="card">
                <div class="card-body">
                  <p class="fourth">
                    <img src={ImgWhy4} alt="" />
                  </p>
                  <h5>Sopir Profesional</h5>
                  <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Testi />

        <div id="advertisement">
          <div class="card">
            <div class="card-body d-flex align-items-center justify-content-center">
              <div class="text-center text-white">
                <h5 class="mb-4">Sewa Mobil di Lombok Tengah Sekarang</h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                  <br /> tempor incididunt ut labore et dolore magna aliqua.{' '}
                </p>
                <button class="btn btn-success">Mulai Sewa Mobil</button>
              </div>
            </div>
          </div>
        </div>
        <div class="container" id="frequently-asked">
          <div class="row">
            <div class="col-4">
              <h2>Frequently Asked Question</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </div>
            <div class="col-8">
              <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne">
                    <div class="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maiores consequuntur autem dignissimos et, tempora eos nisi quod quaerat pariatur cupiditate, dolor, quis obcaecati mollitia sint officiis tenetur hic neque
                        aspernatur.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo">
                    <div class="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maiores consequuntur autem dignissimos et, tempora eos nisi quod quaerat pariatur cupiditate, dolor, quis obcaecati mollitia sint officiis tenetur hic neque
                        aspernatur.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree">
                    <div class="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maiores consequuntur autem dignissimos et, tempora eos nisi quod quaerat pariatur cupiditate, dolor, quis obcaecati mollitia sint officiis tenetur hic neque
                        aspernatur.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFour">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                  <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour">
                    <div class="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maiores consequuntur autem dignissimos et, tempora eos nisi quod quaerat pariatur cupiditate, dolor, quis obcaecati mollitia sint officiis tenetur hic neque
                        aspernatur.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFive">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      Bagaimana jika terjadi kecelakaan
                    </button>
                  </h2>
                  <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive">
                    <div class="accordion-body">
                      <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maiores consequuntur autem dignissimos et, tempora eos nisi quod quaerat pariatur cupiditate, dolor, quis obcaecati mollitia sint officiis tenetur hic neque
                        aspernatur.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MainLayout>
    </>
  );
};

export default Home;
